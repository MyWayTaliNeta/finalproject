package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;
    FragmentTransaction fragmentTransaction;
    RunningRouteOpenHelper runningRouteOpenHelper;
    private final static String TAG= SearchFragment.class.getSimpleName();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();


        //load default fragment
       fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,new HomeFragment());
       fragmentTransaction.commit();
        getSupportActionBar().setTitle("Welcome to My Way");

        // This is my connection to the db.
        runningRouteOpenHelper = new RunningRouteOpenHelper(this);
     //   initdata();

        System.out.println(TAG);
        Log.d(TAG, "My log is here: ");

    }

    // פה תקרה הפונקציה שקורית רק פעם אחת בחיים להכנסת נתונים לדיבי
    public void initdata() {
            runningRouteOpenHelper.open();
         //   runningRouteOpenHelper.deleteAllData();
            runningRouteOpenHelper.createRunningRoute(new RunningRoute(3,"12.1212,24.121","23.1212,48.11","AroundPark,ByTheSea,Fields"));
            runningRouteOpenHelper.createRunningRoute(new RunningRoute(4,"12.1212,24.121","23.1212,48.11","AroundPark,Fields"));
            runningRouteOpenHelper.createRunningRoute(new RunningRoute(2,"12.1212,24.121","23.1212,48.11","AroundPark,ByTheSea"));

        runningRouteOpenHelper.close();
    }

    @Override //implement click from menu to pages
    public boolean onNavigationItemSelected(MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_container, new HomeFragment());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle("Welcome to My Way");
        }

         if (id == R.id.nav_search) {

             fragmentTransaction = getSupportFragmentManager().beginTransaction();
             fragmentTransaction.replace(R.id.main_container, new SearchFragment());
             fragmentTransaction.commit();
             getSupportActionBar().setTitle("Search Route");
         }

        if (id == R.id.nav_create) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_container, new CreateMap());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle("Create your Map");
        }
        if (id == R.id.nav_history) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_container, new HistoryFragment());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle("My History");
        }
        if (id == R.id.nav_favorite) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_container, new HomeFragment());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle("Favorite Tracks");
        }
        if (id == R.id.nav_partner) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_container, new HomeFragment());
            fragmentTransaction.commit();
            getSupportActionBar().setTitle("Find a Partner");
        }
        return true;
    }


}

