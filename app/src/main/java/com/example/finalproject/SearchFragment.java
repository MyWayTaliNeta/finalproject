package com.example.finalproject;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SearchFragment extends Fragment {

    Button btnFind;
   RunningRouteOpenHelper runningRouteOpenHelper;
    private EditText km_number;
    private CheckBox AroundPark, ByTheSea, Uphills, PlainRoute, RoundRoute, Fields, MainStreet, SideStreets;
    ArrayList<RunningRoute> res = new ArrayList<>();
    private final static String TAG= SearchFragment.class.getSimpleName();
    String [] conditions = new String[]{""};
    String total="";
    int count;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      runningRouteOpenHelper = new RunningRouteOpenHelper(this.getContext());
        // Inflate the layout for this fragmen
        View v = inflater.inflate(R.layout.search_fragment, container, false);
        km_number = v.findViewById(R.id.km_number);



        AroundPark = v.findViewById(R.id.AroundPark);
        ByTheSea = v.findViewById(R.id.ByTheSea);
        Uphills = v.findViewById(R.id.Uphills);
        PlainRoute = v.findViewById(R.id.PlainRoute);
        RoundRoute = v.findViewById(R.id.RoundRoute);
        Fields = v.findViewById(R.id.Fields);
        MainStreet = v.findViewById(R.id.MainStreet);
        SideStreets = v.findViewById(R.id.SideStreets);


        btnFind = v.findViewById(R.id.btnFind);
        btnFind.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Check(getView());
                try {
                  getRunningRecomandations();
               } catch (IllegalAccessException e) {
                    e.printStackTrace();
               }

                Bundle bundle = new Bundle();
                bundle.putString("Key", total);
                Log.d(TAG, "kmnumber: "+km_number);
            //   bundle.putString("checkbox",res);

                ReccomendationRoutes reccomendationRoutes = new ReccomendationRoutes();
                reccomendationRoutes.setArguments(bundle);
                //move to recommendations page:
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, reccomendationRoutes);
                fragmentTransaction.commit();

            }

        });
        return v;

    }

    String msg = "";
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void Check(View v) {

        if (AroundPark.isChecked()) {
            msg = msg + "AroundPark,";
        }

        if (ByTheSea.isChecked()) {
            msg = msg + "ByTheSea,";
        }

        if (Uphills.isChecked()) {
            msg = msg + " Up Hills ,";
        }

        if (PlainRoute.isChecked()) {
            msg = msg + " Plain Route ,";
        }

        if (RoundRoute.isChecked()) {
            msg = msg + " Round Route ,";
        }

        if (Fields.isChecked()) {
            msg = msg + " Fields ,";
        }

        if (MainStreet.isChecked()) {
            msg = msg + " Main Street ,";
        }

        if (SideStreets.isChecked()) {
            msg = msg + " Side Streets ";
        }


    }

    public String getRunningRecomandations() throws IllegalAccessException {
        int i,j,index1,index2,k;
        int[] commacounter=new int[3];
        int[] realcommacounter=new int[10];
        String value= km_number.getText().toString();
        int finalkm=Integer.parseInt(value);
        int counter = 0;
        Log.d(TAG, "final: "+finalkm);
        Log.d(TAG, "msg: "+msg);
        conditions = msg.split(",");
        Log.d(TAG, "conditions: "+conditions);
        res=runningRouteOpenHelper.getAllRunnigRoutesBySearchQuery(finalkm, conditions,"12.1212,24.121");
        if(res==null){
            total= "No recommended routes";
        }
        else {
            for (i=0;i<=res.size()-1;i++)
            {
                count=0;
                String numConInRoute=res.get(i).getConditions();
                char comma=',';
                Log.d(TAG, "numConInRoute: "+numConInRoute);
                for(j=0; j<=numConInRoute.length()-1;j++) {
                    Log.d(TAG, "numConInRoute.length(): "+numConInRoute.length());
                    if (numConInRoute.charAt(j) ==comma) {
                        count++;
                        Log.d(TAG, "count: "+count);
                    }
                }
                commacounter[i]=count;
                realcommacounter[i]=count;
                Log.d(TAG, "commacounter: "+ commacounter[i]);
            }

            for(k=0;k<=realcommacounter.length-1;k++){
                if (commacounter.length>=2){
                    Arrays.sort(commacounter);
                    if(commacounter[0]==realcommacounter[k]){
                        index1=k;
                        Log.d(TAG, "index1: "+ index1);}
                    if(commacounter[1]==realcommacounter[k]){
                        index2=k;
                        Log.d(TAG, "index1: "+ index2);}
                }

            }

            total ="\n"+ res.get(0).getKmNum()+" KM" + "," + res.get(0).getConditions()+"\n"+"\n"+"\n"+"\n"+"\n"+res.get(1).getKmNum()+" KM" + "," + res.get(1).getConditions()+"\n";


        }
        return total;
    }


}