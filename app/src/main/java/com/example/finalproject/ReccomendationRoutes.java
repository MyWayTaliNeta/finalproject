package com.example.finalproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

public class ReccomendationRoutes extends Fragment {
    private TextView view_km,con;
    private CheckBox park;
    Button btnStart1;
    Button btnStart2;
    String [] array;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_reccomendation_routes,container,false);
        Bundle bundle = this.getArguments();
        //moving KM data
        view_km = view.findViewById(R.id.view_km);
        //con = view.findViewById(R.id.tv_con);
        String KMdata = bundle.getString("Key");
       // String msg = bundle.getString("checkbox");
       // array[0]= msg;

      //  RunningRouteOpenHelper runningRouteOpenHelper = new RunningRouteOpenHelper(getActivity());
     ////   ArrayList<RunningRoute> res = runningRouteOpenHelper.getAllRunnigRoutesBySearchQuery(5,array,"32.115133,34.818115");
       view_km.setText(KMdata);
     //  con.setText(msg);

        //view_km.setText(res.get(0));
        //con.setText( res.get(1));


        btnStart1 = view.findViewById(R.id.btnStart1);
        btnStart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecommendMapFragment recommendMapFragment = new RecommendMapFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container,recommendMapFragment);
                fragmentTransaction.commit();

            }
        });
        btnStart2 = view.findViewById(R.id.btnStart2);
        btnStart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecommendMapFragment recommendMapFragment = new RecommendMapFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container,recommendMapFragment);
                fragmentTransaction.commit();

            }
        });
        return view;

    }





}
