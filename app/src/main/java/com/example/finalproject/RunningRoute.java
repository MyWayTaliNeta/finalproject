package com.example.finalproject;

public class RunningRoute {
    private long id;
    private int kmNum;
    private String startPoint;
    private String endPoint;
    private String conditions;

    public RunningRoute(long id, int kmNum, String startPoint, String endPoint, String conditions) {
        this.id = id;
        this.kmNum = kmNum;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.conditions = conditions;
    }

    public RunningRoute(int kmNum, String startPoint, String endPoint, String conditions) {
        this.id = 0;
        this.kmNum = kmNum;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.conditions = conditions;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getKmNum() {
        return kmNum;
    }

    public void setKmNum(int kmNum) {
        this.kmNum = kmNum;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }
}
