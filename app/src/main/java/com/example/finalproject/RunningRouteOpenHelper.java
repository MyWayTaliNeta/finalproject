package com.example.finalproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class RunningRouteOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "myWay.db";
    public static final String TABLE_RUNNING_ROUTES = "runningRoutes";
    public static final int DATABASEVERSION = 1;

    public static final String COLUMN_ID = "RouteId";
    public static final String COLUMN_KM_NUM = "KmNum";
    public static final String COLUMN_START_POINT = "StartPoint";
    public static final String COLUMN_END_POINT = "EndPoint";
    public static final String COLUMN_COND = "Conditions";

    private final static String TAG= RunningRouteOpenHelper.class.getSimpleName();


    private static final String CREATE_TABLE_RUNNING_ROUTES = "CREATE TABLE IF NOT EXISTS " +
            TABLE_RUNNING_ROUTES + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_KM_NUM + " INTEGER," +
            COLUMN_START_POINT + " TEXT," +
            COLUMN_END_POINT + " TEXT," +
            COLUMN_COND + " TEXT " + ");";

    String[] allColumns = {
            COLUMN_ID,
            COLUMN_KM_NUM,
            COLUMN_START_POINT,
            COLUMN_END_POINT,
            COLUMN_COND
    };

    SQLiteDatabase db;


    public RunningRouteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASEVERSION);
    }

    // Everytime when we want information from db we have to open connection. and when we finished to close.
    public void open() {
        db = this.getWritableDatabase();
        Log.d("runningRoutes", "database connection open");

    }
    // Create the table in our db.
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_RUNNING_ROUTES);
        Log.d("runningRoutes", "Table runningRoutes created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(String.format("DROP TABLE IF EXISTS%s", TABLE_RUNNING_ROUTES));
        onCreate(db);
    }

    //adding new RunningRoute to the database and get his ordinary id
    public RunningRoute createRunningRoute(RunningRoute runningRoute) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_KM_NUM, runningRoute.getKmNum());
        values.put(COLUMN_START_POINT, runningRoute.getStartPoint());
        values.put(COLUMN_END_POINT, runningRoute.getEndPoint());
        values.put(COLUMN_COND , runningRoute.getConditions());

        // when we insert to db , db gives us the id.
        long insertId = db.insert(TABLE_RUNNING_ROUTES, null, values);
        Log.d("runningRoutes", "runningRoute id # " + insertId + " insert to database");
        runningRoute.setId(insertId);
        return runningRoute;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_RUNNING_ROUTES,null);
        return res;
    }

    public int deleteAllData ()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_RUNNING_ROUTES,null,null);
    }

    // copy all routes from db to java RunningRoutes array list.
    public ArrayList<RunningRoute> getAllRunnigRoutes() {
        ArrayList<RunningRoute> list = new ArrayList<RunningRoute>();
        open();
        Cursor cursor = db.query(TABLE_RUNNING_ROUTES, allColumns, null, null, null, null, null);
        Log.d(TAG, "count: "+cursor.getCount());
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                int kmNum = cursor.getInt(cursor.getColumnIndex(COLUMN_KM_NUM));
                String startPoint = cursor.getString(cursor.getColumnIndex(COLUMN_START_POINT));
                String endPoint = cursor.getString(cursor.getColumnIndex(COLUMN_END_POINT));;
                String conditions = cursor.getString(cursor.getColumnIndex(COLUMN_COND));;

                list.add(new RunningRoute(id,kmNum,startPoint,endPoint,conditions));
            }
        }
        Log.d(TAG, "getAllRunnigRoutes: "+list.get(0).getConditions());
        Log.d(TAG, "getAllRunnigRoutes: "+list.get(0).getEndPoint());
        Log.d(TAG, "getAllRunnigRoutes: "+list.get(0).getStartPoint());
        Log.d(TAG, "getAllRunnigRoutes: "+list.get(0).getKmNum());
        Log.d(TAG, "getAllRunnigRoutes: "+list.get(0).getId());

        close();


        return list;
    }

    public ArrayList<RunningRoute> getAllRunnigRoutesBySearchQuery(int km, String[] conditions , String startFrom) {
              Log.d(TAG, "input: "+km+","+conditions[0]+","+startFrom);

        ArrayList<RunningRoute> filtered = new ArrayList<>();
        int conditionCounter = 0;
        LatLng currentRouteConverted;
        LatLng startFromConverted;
        for(RunningRoute runningRoute: this.getAllRunnigRoutes()) {
            Log.d(TAG, "km: "+runningRoute.getKmNum());
            if(Math.abs(runningRoute.getKmNum() - km) <= 1 ) {
                Log.d(TAG, "ok: ");

                for (String condition: conditions) {
                    Log.d(TAG, "string: "+conditions[0]);

                    if(runningRoute.getConditions().contains(condition)) {
                        conditionCounter++;
                        Log.d(TAG, "conditionCounter: "+conditionCounter);

                    }
                }
                if(conditionCounter >= 2) {
                    currentRouteConverted = convertStringPointToLatlang(runningRoute.getStartPoint());
                    startFromConverted = convertStringPointToLatlang(startFrom);
                    Log.d(TAG, "Sconverted: "+startFromConverted);
                    Log.d(TAG, "Sconverted: "+currentRouteConverted);
                    if(isDistanceIsUnderMinDistance(currentRouteConverted, startFromConverted)) {
                        filtered.add(runningRoute);
                    }
                }
            }
            else
            {
                return null;
            }


        }
        Log.d(TAG, "filtercount: "+filtered.size());
        Log.d(TAG, "filter: "+filtered.get(0).getKmNum());
      Log.d(TAG, "filter: "+filtered.get(0).getConditions());
        Log.d(TAG, "filtercount: "+filtered.size());

        return filtered;
    }

    // change String to x , y
    // TODO : check to function for real with database information.
    public LatLng convertStringPointToLatlang (String toConvert) {
        int commaIndex = toConvert.indexOf(",")+1;
        Log.d(TAG, "comma: "+commaIndex);

        return new LatLng(Double.valueOf(toConvert.substring(commaIndex)), Double.valueOf(toConvert.substring(commaIndex, toConvert.length() - 1)));
    }

    // Checking distance function between startPoint and startFrom
    public boolean isDistanceIsUnderMinDistance(LatLng from , LatLng startPoint) {
        return Math.sqrt(Math.pow(from.latitude - startPoint.latitude,2) +  Math.pow(from.longitude - startPoint.longitude,2)) <= 2;
    }
}

